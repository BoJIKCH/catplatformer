﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour {

    public virtual void RecieveDamage()
    {
        Die();
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }
    
}
