﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : UnitController {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        UnitController unit = collision.GetComponent<UnitController>();

        if (unit && unit as CharacterController)
        {
            unit.RecieveDamage();
        }
    }
}
