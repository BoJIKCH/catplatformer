﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : UnitController {

    protected virtual void Awake() { }
    protected virtual void Start() { }
    protected virtual void Update() { }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        BulletController bullet = collision.GetComponent<BulletController>();
        UnitController unit = collision.GetComponent<UnitController>();

        if (bullet)
        {
            RecieveDamage();
        }

        if (unit && unit is CharacterController)
        {
            unit.RecieveDamage();
        }
    }

}
