﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterController : UnitController
{

    [SerializeField]
    private float speed = 0.1F;

    [SerializeField]
    private float jumpForce = 6.5F;

    [SerializeField]
    private int lives = 5;

    private LivesBarController livesBar;

    public int Lives
    {
        get { return lives; }
        set
        {
            if (value < 5) lives = value;
            livesBar.Refresh();
        }
    }

    private bool isGrounded = false;

    private float collisionDir;

    private CharacterState State
    {
        get { return (CharacterState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int) value); }
    }

    private Rigidbody2D rigidbody;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private BulletController bullet;

    private Buttons btnState;

    void Awake()
    {
        livesBar = FindObjectOfType<LivesBarController>();
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        bullet = Resources.Load<BulletController>("Bullet");
        btnState = Buttons.None;
    }

    // Use this for initialization
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        if (transform.position.y < -4.5F)
        {
            Die();
        }

        if (isGrounded)
        {
            State = CharacterState.Idle;
        }
        
        if (Input.GetButton("Horizontal"))
        {
            Run();
        }
        
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Jump();
        }
        /*
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
        */
        // Mobile

        if (btnState == Buttons.Left) RunNoAxis(-1);
        if (btnState == Buttons.Right) RunNoAxis(1);


    }

    public void MoveStop()
    {
        btnState = Buttons.None;
    }

    public void MoveLeft()
    {
        btnState = Buttons.Left;
    }

    public void MoveRight()
    {
        btnState = Buttons.Right;
    }

    public void MoveJump()
    {
        if (isGrounded)
        {
            Jump();
        }
    }


    private void FixedUpdate()
    {
        CheckGrounded();
    }

    private void RunNoAxis(int axis)
    {
        Vector3 direction = transform.right * axis;

        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed + Time.deltaTime);

        spriteRenderer.flipX = direction.x < 0.0F;

        if (isGrounded)
        {
            State = CharacterState.Run;
        }
    }

    private void Run()
    {
        Vector3 direction = transform.right * Input.GetAxis("Horizontal");

        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed + Time.deltaTime);

        spriteRenderer.flipX = direction.x < 0.0F;

        if (isGrounded)
        {
            State = CharacterState.Run;
        }
    }


    public void Jump()
    {
        rigidbody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }

    private void CheckGrounded()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.1F);

        isGrounded = colliders.Length > 1;

        if (!isGrounded)
        {
            State = CharacterState.Jump;
        }
    }

    public void Shoot()
    {
        Vector3 position = transform.position;
        position.y += 0.7F;

        BulletController playerBuller = Instantiate(bullet, position, bullet.transform.rotation) as BulletController;

        playerBuller.Parent = gameObject;
        playerBuller.Direction = playerBuller.transform.right * (spriteRenderer.flipX ? -1.0F : 1.0F);
    }

    public override void RecieveDamage()
    {/*
        float axis = 0.0F;

        if (Input.GetAxis("Horizontal") != 0.0F) axis = -(Input.GetAxis("Horizontal"));

        if (btnState == Buttons.Right)  axis = -1.0F; else if (btnState == Buttons.Left) axis = 1.0F; 
        */
        Lives--;

        rigidbody.velocity = Vector3.zero;
        rigidbody.AddForce(transform.up * 4.0F, ForceMode2D.Impulse);
        rigidbody.AddForce(transform.right * collisionDir * 3.5F, ForceMode2D.Impulse);

        if (lives == 0)
        {
            Die();
        }
    }

    public override void Die()
    {
        base.Die();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        UnitController unit = collision.gameObject.GetComponent<UnitController>();
        if (unit)
        {
            collisionDir = transform.position.x - unit.transform.position.x > 0 ? 1.0F : -1.0F;
        }
    }


}

public enum CharacterState
{
    Idle,
    Run,
    Jump
}

public enum Buttons
{
    None, 
    Left, 
    Right, 
    Jump
}