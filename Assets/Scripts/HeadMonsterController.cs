﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadMonsterController : MonsterController {

    private BulletController bullet;

    protected override void Awake()
    {
        bullet = Resources.Load<BulletController>("Bullet");
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        UnitController unit = collision.GetComponent<UnitController>();

        if (unit && unit is CharacterController)
        {
            if (Mathf.Abs(unit.transform.position.x - transform.position.x) < 1.0F)
            {
                RecieveDamage();
            }
            else
            {
                unit.RecieveDamage();
            }
        }
    }

    // Use this for initialization
    protected override void Start() { }
	
	// Update is called once per frame
	protected override void Update () { }
}
