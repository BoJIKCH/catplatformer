﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float speed = 20.0F;

    private SpriteRenderer spriteRenderer;

    private Vector3 direction;

    public Vector3 Direction
    {
        set { direction = value; }
    }

    private GameObject parent;
    public GameObject Parent
    {
        set { parent = value; }
    }

	// Use this for initialization
	void Start ()
    {
        Destroy(gameObject, 0.3F);
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
	}

    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        UnitController unit = collision.GetComponent<UnitController>();

        if (unit && unit.gameObject != parent)
        {
            Debug.Log("11");
            Destroy(gameObject);
        }
    }
}
