﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private float speed = 2.0F;

    [SerializeField]
    private Transform target;

    // Use this for initialization
    void Start () {
		
	}

    private void Awake()
    {
       if (!target)
        {
            target = FindObjectOfType<CharacterController>().transform;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (target)
        {
            Vector3 position = target.position;
            position.z = -10.0F;
            position.y += 0.8F;

            transform.position = Vector3.Lerp(transform.position, position, speed * Time.deltaTime);
        }
	}
}
